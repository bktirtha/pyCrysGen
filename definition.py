### Author: Bishwajit Kar
### License: MIT

## Definition
import numpy as np

### Crystal Structure Definition (Basis + Motif)

def main():
    print("Import this file in your script to access the Crystal class!")

class Crystal():
	def __init__(self):
		self.__system_size:np.ndarray = None
		self.reduced:bool = True
		self.lat_param:float|tuple = None
		self.basis:np.ndarray = None
		self.base_atoms:np.ndarray = None
		self.atom_type:np.ndarray = None
		self.buck:float|None = None

	def set_system_size(self,size:list,reduced:bool=True):
		self.__system_size = np.array(size)
		self.reduced = reduced
	def get_system_size(self):
		return self.__system_size

	def FCC(self,lat_param:float):
		self.lat_param = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
						  [0.0, 1.0, 0.0],
						  [0.0, 0.0, 1.0]],dtype=np.float64)*lat_param  # 	assuming an cubic cell starting at the origin
		self.base_atoms =  np.array([[0.0, 0.0, 0.0],
							    	 [1.0/2.0, 1.0/2.0, 0.0],
							         [1.0/2.0, 0.0, 1.0/2.0],
									 [0.0, 1.0/2.0, 1.0/2.0]],dtype=np.float64)
		self.atom_type = np.array([1,1,1,1])

	def BCC(self,lat_param:float):
		self.lat_param = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
						  [0.0, 1.0, 0.0],
						  [0.0, 0.0, 1.0]],dtype=np.float64)*lat_param  # 	assuming an cubic cell starting at the origin
		self.base_atoms =  np.array([[0.0, 0.0, 0.0],
							    	 [1.0/2.0, 1.0/2.0, 1.0/2.0]],dtype=np.float64)
		self.atom_type = np.array([1,1])

	def zb(self,lat_param:float):   #zinc-blende system
		self.lat_param = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
						  [0.0, 1.0, 0.0],
						  [0.0, 0.0, 1.0]],dtype=np.float64)*lat_param
		self.base_atoms =  np.array([[0.0, 0.0, 0.0],
							    [1.0/2.0, 1.0/2.0, 0.0],
							    [1.0/2.0, 0.0, 1.0/2.0],
							    [0.0, 1.0/2.0, 1.0/2.0],
				# zb portion
								[3.0/4.0, 3.0/4.0, 3.0/4.0],  
								[1.0/4.0, 3.0/4.0, 1.0/4.0],
								[1.0/4.0, 1.0/4.0, 3.0/4.0],
								[3.0/4.0, 1.0/4.0, 1.0/4.0]],dtype=np.float64)
		self.atom_type = np.array([1]*4 + [2]*4)

	def wurt(self,lat_param:tuple):
		self.lat_param = lat_param
		a,c = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
					  	  [-1/2, np.sqrt(3.0)/2.0, 0.0],
					      [0.0, 0.0, c/a]],dtype=np.float64)*a
		self.base_atoms = np.array([[0.0, 0.0, 0.0],
						 	   [1.0/3.0, 2.0/3.0, 1.0/2.0],
							   [0.0, 0.0, 3.0/8.0], #wz portion
							   [1.0/3.0, 2.0/3.0, 7.0/8.0]],dtype=np.float64)
		self.atom_type = np.array([1]*2+[2]*2)

	def Graphene(self,lat_param:float = 2.464, buck:float=0.0):
		self.lat_param = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
					  [-1.0/2.0, np.sqrt(3.0)/2.0, 0.0],
					  [0.0, 0.0, 30.0/lat_param]],dtype=np.float64)*lat_param
		self.buck = buck
		buck =  buck/self.basis[2,2]
		self.base_atoms = np.array([[1.0/3.0, 2.0/3.0, 0.0],
						   	   [2.0/3.0, 1.0/3.0, buck]],dtype=np.float64)

		self.atom_type = np.array([1]*2)

	def TMD(self,lat_param:float = 2.464, buck:float=0.0):
		self.lat_param = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
					  [-1.0/2.0, np.sqrt(3.0)/2.0, 0.0],
					  [0.0, 0.0, 30.0/lat_param]],dtype=np.float64)*lat_param
		self.buck = buck
		buck =  buck/self.basis[2,2]
		self.base_atoms = np.array([[1.0/3.0, 2.0/3.0, buck],
						   	   [2.0/3.0, 1.0/3.0, 2*buck],
						   	   [2.0/3.0, 1.0/3.0, 0.0]],dtype=np.float64)
		self.atom_type = np.array([1,2,3])

	def OrthoGraphene(self,lat_param:float = 2.464, buck:float=0.0):
		self.lat_param = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
					  [0.0, np.sqrt(3.0), 0.0],
					  [0.0, 0.0, 30.0/lat_param]],dtype=np.float64)*lat_param
		self.buck = buck
		buck = buck/self.basis[2,2]
		self.base_atoms = np.array([ [1.0/2.0, 5.0/6.0, 0.0],
						   		[0.0, 1.0/3.0, 0.0],
								[1.0/2.0, 1.0/6.0, buck],
						   		[0.0, 2.0/3.0, buck]],dtype=np.float64)
		self.atom_type = np.array([1]*4)

	def OrthoTMD(self,lat_param:float = 2.464, buck:float=1.6):
		self.lat_param = lat_param
		self.basis = np.array([[1.0, 0.0, 0.0],
					  [0.0, np.sqrt(3.0), 0.0],
					  [0.0, 0.0, 100.0/lat_param]],dtype=np.float64)*lat_param
		self.buck = buck
		buck = buck/self.basis[2,2]
		self.base_atoms = np.array([
					   		[0.0, 1.0/3.0, buck], #1
							[1.0/2.0, 1.0/6.0, 2*buck], #2
							[1.0/2.0, 1.0/6.0, 0.0], #3
						    [1.0/2.0, 5.0/6.0, buck], #1
					   		[0.0, 2.0/3.0, 2*buck], #2
					   		[0.0, 2.0/3.0, 0.0]  #3
							],dtype=np.float64)
		self.atom_type = np.array([1,2,3,1,2,3])

def Generate_Crystal(Crystal:Crystal) -> tuple:
	if not Crystal.reduced:
		raise Exception("Must Use Reduced Size Parameters!")
	system_size = Crystal.get_system_size()
	# Generate basis points
	x = np.arange(system_size[0],dtype=np.float64)
	y = np.arange(system_size[1],dtype=np.float64)
	z = np.arange(system_size[2],dtype=np.float64)
	xx,yy,zz = np.meshgrid(x,y,z)
	gridpts = np.stack((xx.ravel(),yy.ravel(),zz.ravel()),axis=1)
	# Create Lattice
	latpts = np.matmul(gridpts,Crystal.basis)
	base_atoms_cart = np.matmul(Crystal.base_atoms,Crystal.basis)
	# Getting Coordinates
	atom_coord = np.array([i+base_atoms_cart for i in latpts]).reshape(-1,3)
	# Adjusting numerical errors
	atom_coord = np.where(np.abs(atom_coord) <= 1e-5, 0.0, atom_coord) 
	atom_type_array = np.tile(Crystal.atom_type,int(len(atom_coord)/len(Crystal.atom_type)))
	return atom_coord,atom_type_array


### Transformation
def rotate(Crystal:Crystal,axis:list, angle:float) -> np.ndarray:
	a = np.cos(angle/2.0)
	s = np.sin(angle/2.0)
	b = axis[0]*s
	c = axis[1]*s 
	d = axis[2]*s 
	rot =  np.array([[a*a + b*b - c*c - d*d, 2*(b*c - a*d), 2*(b*d+a*c)],
					 [2*(b*c+a*d), a*a+c*c-b*b-d*d, 2*(c*d-a*b)],
					 [2*(b*d-a*c), 2*(c*d+a*b), a*a+d*d-b*b-c*c]],dtype=np.float64)
	transform_matrix = np.matmul(Crystal.basis,rot.T)
	return transform_matrix

def crystal_orient(Crystal:Crystal,new_basis:list) -> np.ndarray:
	new_basis = np.array(new_basis,dtype=np.float64)
	norm_fact = np.sqrt(np.sum(new_basis**2,axis=1)).reshape(-1,1)
	new_basis = new_basis/norm_fact
	inv_new_basis = np.linalg.inv(new_basis)
	transform_matrix = np.matmul(Crystal.basis,inv_new_basis)
	return transform_matrix

def Generate_Crystal_Transform(Crystal:Crystal,transform_matrix:np.ndarray) -> tuple:
	from itertools import product
	system_size = Crystal.get_system_size()
	if not Crystal.reduced:
		system_size_cart = system_size
	else:
		system_size_cart = np.matmul(np.array(system_size,dtype=np.float64),Crystal.basis)
	
	# global transform_matrix,basis,base_atoms,system_size
	#boundary determination
	systembasis = np.identity(3)*system_size_cart
	invsysbasis = np.linalg.inv(systembasis) #needed for fractional array later
	inv_transform = np.linalg.inv(transform_matrix) 
	base_atoms_cart = np.matmul(Crystal.base_atoms, transform_matrix)  #points_will_transform
	corners_basis = np.array(list(product((0,system_size_cart[0]),(0,system_size_cart[1]),(0,system_size_cart[2])))) #basis coord
	corners_vec = np.matmul(corners_basis,inv_transform)  #same_points_in_different_basis
	mins = [int(np.min([m,0])) for m in np.floor(np.min(corners_vec,axis=0))]
	maxs = [int(np.max([m,0]))+1 for m in np.ceil(np.max(corners_vec,axis=0))]

	#coord generation
	x = np.arange(mins[0],maxs[0])
	y = np.arange(mins[1],maxs[1])
	z = np.arange(mins[2],maxs[2])
	xv,yv,zv = np.meshgrid(x,y,z)
	xv,yv,zv = xv.ravel(),yv.ravel(),zv.ravel()
	gridpts = np.stack((xv,yv,zv),axis=1)
	latpts = np.matmul(gridpts,transform_matrix)
	atom_coord = np.array([i+base_atoms_cart for i in latpts]).reshape(-1,3)
	atom_type_track = np.tile(Crystal.atom_type,int(len(atom_coord)/len(Crystal.atom_type)))
	
	#adjusting numerical errors
	thr = 1e-5
	atom_coord = np.where(np.abs(atom_coord) <= thr, 0, atom_coord) 
	sysfrac = np.matmul(atom_coord,invsysbasis)
	indices = np.where(np.all((sysfrac>=0) & (sysfrac<(1-thr)),axis=1))

	#deleting atoms outside system_size
	positions = atom_coord[indices[0]]
	atom_type_array = atom_type_track[indices[0]]
	return positions,atom_type_array

def Write_LAMMPS_output(Crystal:Crystal,filename:str,position_type:tuple|None = None):
	if position_type is None:
		positions,atom_type_array = Generate_Crystal(Crystal)
	else:
		positions,atom_type_array = position_type
	
	system_size = Crystal.get_system_size()
	if not Crystal.reduced:
		xhi,yhi,zhi = system_size
		tf_xy,tf_xz,tf_yz = [0.0,0.0,0.0]
	else:
		xhi,yhi,zhi = np.array([Crystal.basis[0,0],Crystal.basis[1,1],Crystal.basis[2,2]])*np.array(system_size)
		tf_xy,tf_xz,tf_yz = np.array([Crystal.basis[1,0],Crystal.basis[2,0],Crystal.basis[2,1]])*np.array(system_size)[[1,2,2]]

	# Write LAMMPS data file
	with open(filename,'w') as fdata:
		# First line is a comment line 
		fdata.write('# Python Generated Input file\n\n')
		#--- Header ---#
		# Specify number of atoms and atom types 
		fdata.write('{} atoms\n'.format(len(positions)))
		fdata.write('{} atom types\n'.format(len(set(Crystal.atom_type))))
		# Specify box dimensions
		fdata.write('{} {} xlo xhi\n'.format(0.0, xhi))
		fdata.write('{} {} ylo yhi\n'.format(0.0, yhi))
		fdata.write('{} {} zlo zhi\n'.format(0.0, zhi))
		fdata.write('{} {} {} xy xz yz\n'.format(tf_xy,tf_xz,tf_yz))
		fdata.write('\n')

		# Atoms section
		fdata.write('Atoms\n\n')

		# Write each position 
		for i,pos in enumerate(positions):
				fdata.write('{} {} {} {} {}\n'.format(i+1,atom_type_array[i],*pos))

def NT_Generate(Crystal:Crystal,chi_idx:tuple,length:float,filename:str):
	n,m = chi_idx
	
	#Dimensions from theory (www.photon.t.u-tokyo.ac.jp/~maruyama/kataura/chirality.html)
	width = Crystal.lat_param*np.sqrt(m**2+m*n+n**2)
	NT_dia = width/np.pi

	buck = Crystal.buck


	#For Maintaining Periodicity along NT axis (y axis)
	L_ = np.sqrt(3)*width/np.gcd(2*m+n,2*n+m) # N_ = np.gcd(2*m+n,2*n+m)
	print(f"Periodic Length: {L_}")
	length = np.round(length/L_)*L_

	#Final System Settings
	Crystal.set_system_size([width,length,30],reduced=False)
	center = np.array([width/2,length/2,(buck + NT_dia/2)])
	r0 = NT_dia/2

	#Transformation
	axis = [0,0,1]
	angle = np.arctan(np.sqrt(3)*m/(m+2*n))
	print(f"Chiral Angle: {angle*180/np.pi}\nRadius: {r0}\nLength: {length}")

	#Rotating 2D Sheet to Enable Chirality
	tf = rotate(Crystal,axis,angle)         
	posit,type_array = Generate_Crystal_Transform(Crystal,tf)
	

	#Rolling Up the Sheet into Cylinder
	xx,yy,zz = [],[],[]
	for i in posit:
		r = abs(center[2]-i[2])
		t = (i[0]-center[0])/r0
		x_ = r*np.cos(t) + center[0]
		z_ = r*np.sin(t) + center[2]
		y_ = i[1]
		xx.append(x_),yy.append(y_),zz.append(z_)
	nt_pos = np.stack((np.array(xx),np.array(yy),np.array(zz)),axis=1) + np.array([0.0,0.0,0.5*width - r0])

	#Create Output with Sensible Name
	# if m==0:
	# 	chi="zig"
	# elif m==n:
	# 	chi="arm"
	# else:
	# 	chi="chi"
	
	# Adjust before writing
	Crystal.set_system_size([width,length,width],reduced=False) 
	# Write_LAMMPS_output(Crystal,filename=f"nanotube_{chi}_{n}.{m}.data",position_type=(nt_pos,type_array))
	Write_LAMMPS_output(Crystal,filename=f"{filename}",position_type=(nt_pos,type_array))

if __name__ == "__main__":
    main()
