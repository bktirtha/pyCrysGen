# PyCrysGen: A Python Project

## Overview

This project provides Python classes and functions for creating crystals of various types, including FCC, BCC, HCP, ZB, WZ, and TMD structures. It also supports the generation of 2D materials and nanotubes.

## Features

- Generate crystals in FCC, BCC, HCP, ZB, WZ, TMD, and other structures.
- Create 2D materials and nanotubes.
- Change crystal orientation and rotate a crystal around a specific axis.
- Output files in the LMP format (LAMMPS specific);support for additional formats will be added later.

## Usage

Open the provided jupyter notebook file "CrysGen.ipynb" to understand basic usage.
 **Note: You have to assign atoms to each lammps 'atom type' in the input script** 
